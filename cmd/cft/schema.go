package main

import (
	"github.com/spf13/cobra"
	"gitlab.com/oxxygen/porter-cft/pkg/cft"
)

func buildSchemaCommand(m *cft.Mixin) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "schema",
		Short: "Print the json schema for the mixin",
		RunE: func(cmd *cobra.Command, args []string) error {
			return m.PrintSchema()
		},
	}
	return cmd
}
