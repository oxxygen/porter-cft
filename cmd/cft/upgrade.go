package main

import (
	"github.com/spf13/cobra"
	"gitlab.com/oxxygen/porter-cft/pkg/cft"
)

func buildUpgradeCommand(m *cft.Mixin) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "upgrade",
		Short: "Execute the invoke functionality of this mixin",
		RunE: func(cmd *cobra.Command, args []string) error {
			return m.Execute()
		},
	}
	return cmd
}
