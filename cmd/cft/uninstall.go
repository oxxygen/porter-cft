package main

import (
	"github.com/spf13/cobra"
	"gitlab.com/oxxygen/porter-cft/pkg/cft"
)

func buildUninstallCommand(m *cft.Mixin) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "uninstall",
		Short: "Execute the uninstall functionality of this mixin",
		RunE: func(cmd *cobra.Command, args []string) error {
			return m.Execute()
		},
	}
	return cmd
}
