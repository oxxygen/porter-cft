package cft

import "fmt"

// This is an example. Replace the following with whatever steps are needed to
// install required components into
const dockerfileLines = `RUN apt-get update && apt-get -y install make python3 python3-pip golang build-essential python3-venv
RUN ln -sf /usr/bin/python3 /usr/bin/python
RUN pip3 install --upgrade pip
RUN pip3 install setuptools 
WORKDIR /
RUN git clone https://github.com/GoogleCloudPlatform/cloud-foundation-toolkit
RUN echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] http://packages.cloud.google.com/apt cloud-sdk main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list && curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key --keyring /usr/share/keyrings/cloud.google.gpg  add - && apt-get update -y && apt-get install google-cloud-sdk -y
RUN wget https://dl.google.com/go/go1.13.9.linux-amd64.tar.gz 
RUN tar xf go1.13.9.linux-amd64.tar.gz 
RUN mv go /usr/local/go-1.13 
ENV GOROOT /usr/local/go-1.13
ENV PATH=$GOROOT/bin:$PATH
RUN cd cloud-foundation-toolkit/ && cd dm && make cft-prerequisites && make build && make install
RUN cd /cloud-foundation-toolkit/ && cd cli && make build
RUN cd /cloud-foundation-toolkit/dm && make cft-venv
ENV VIRTUAL_ENV=/cloud-foundation-toolkit/dm/venv
ENV PATH="$VIRTUAL_ENV/bin:$PATH"
`

// Build will generate the necessary Dockerfile lines
// for an invocation image using this mixin
func (m *Mixin) Build() error {
	// fmt.Fprintf(m.Out, dockerfileLines)
	fmt.Fprintln(m.Out, `FROM debian`)
	fmt.Fprintln(m.Out, `ARG BUNDLE_DIR`)
	fmt.Fprintln(m.Out, `RUN apt-get update && apt-get -y install make python3 python3-pip golang build-essential python3-venv git curl wget`)
	fmt.Fprintln(m.Out, `RUN ln -sf /usr/bin/python3 /usr/bin/python`)
	fmt.Fprintln(m.Out, `RUN pip3 install --upgrade pip`)
	fmt.Fprintln(m.Out, `RUN pip3 install setuptools `)
	fmt.Fprintln(m.Out, `WORKDIR /`)
	fmt.Fprintln(m.Out, `RUN git clone https://github.com/GoogleCloudPlatform/cloud-foundation-toolkit`)
	fmt.Fprintln(m.Out, `RUN echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] http://packages.cloud.google.com/apt cloud-sdk main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list && curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key --keyring /usr/share/keyrings/cloud.google.gpg  add - && apt-get update -y && apt-get install google-cloud-sdk -y`)
	fmt.Fprintln(m.Out, `RUN wget https://dl.google.com/go/go1.13.9.linux-amd64.tar.gz `)
	fmt.Fprintln(m.Out, `RUN tar xf go1.13.9.linux-amd64.tar.gz `)
	fmt.Fprintln(m.Out, `RUN mv go /usr/local/go-1.13 `)
	fmt.Fprintln(m.Out, `ENV GOROOT /usr/local/go-1.13`)
	fmt.Fprintln(m.Out, `ENV PATH=$GOROOT/bin:$PATH`)
	fmt.Fprintln(m.Out, `RUN cd cloud-foundation-toolkit/ && cd dm && make cft-prerequisites && make build && make install`)
	fmt.Fprintln(m.Out, `RUN cd /cloud-foundation-toolkit/ && cd cli && make build`)
	fmt.Fprintln(m.Out, `RUN cd /cloud-foundation-toolkit/dm && make cft-venv`)
	fmt.Fprintln(m.Out, `ENV VIRTUAL_ENV=/cloud-foundation-toolkit/dm/venv`)
	fmt.Fprintln(m.Out, `ENV PATH="$VIRTUAL_ENV/bin:$PATH"`)
	return nil
}
